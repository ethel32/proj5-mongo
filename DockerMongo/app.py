import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times
import arrow

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.route('/display', methods=['GET', 'POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) > 0:
        return render_template('display.html', items=items)
    else:
        return render_template("error.html")
    

@app.route('/submit', methods=['POST'])
def submit():
    openTimes = request.form.getlist("open")
    closeTimes = request.form.getlist("close")
    locations = request.form.getlist("location")
    distKMs = request.form.getlist("km")
    distMs = request.form.getlist("miles")
    noteses = request.form.getlist("notes")

    count = 0
    item_docs = []
    # parameters = ["open", "close", "location", "km", "miles", "notes"]
    for i in range(len(openTimes)):
        if openTimes[i] != "":
            count += 1
            item_doc = {
                'open': openTimes[i],
                'close': closeTimes[i] if len(closeTimes) > i else "",
                'locations': locations[i] if len(locations) > i else "",
                'distKM': distKMs[i] if len(distKMs) > i else "",
                'distM': distMs[i] if len(distMs) > i else "",
                'notes': noteses[i] if len(noteses) > i else ""
            }
            item_docs.append(item_doc)
    
    if count == 0:
        return render_template("error.html")
    else:
        for item in item_docs:
            db.tododb.insert_one(item)
    
    return render_template("calc.html")

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    notes = ""

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    brevet_dist_km = request.args.get("brevetDistKm", 200, type=int)
    begin_time = request.args.get("beginTime", "", type=str)
    date = request.args.get("beginDate", "", type=str)
    
    # formatting to be passed into open & close time
    start_iso = arrow.get("{} {}".format(date, begin_time)).isoformat()
    
    # get open and close times
    open_time = acp_times.open_time(km, brevet_dist_km, start_iso)
    close_time = acp_times.close_time(km, brevet_dist_km, start_iso)

    # error handling (warnings taken from the ACP calculator)
    percentWarn = (km > 0 and (((km - brevet_dist_km) / km) > 0.2))
    zeroWarn = (km == 0)

    if zeroWarn:
        notes = "The distance is zero"
    
    if percentWarn:
        notes = "The distance is 20 percent or longer than a standard brevet"

    return flask.jsonify(result={"open": open_time, "close": close_time, "notes": notes})

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
