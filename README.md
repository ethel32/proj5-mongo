# ACP Controle Times calculator
by Ethel Arterberry (earterb3@uoregon.edu) with database functionality

Reimplemented the RUSA ACP controle time calculator with flask, ajax, and mongodb

Credits to Michal Young for the initial version of this code.

# Purpose
This calculates the open and close times for standard ACP brevets with lengths of 200km, 300km, 400km, 600km and 1000km. It allows you to input checkpoints into a web inteface.

- Rounds seconds in a standard fashion.
- Error messages from the original calculator are kept and passed into the notes. If the control is between 0 and 20% longer than a brevet, it will be rounded to the nearest brevet. If it's 20% or longer, a message will be thrown

Controle times are calculated according to this table

|Location(km)|Min speed (km/hr)|Max speed(km/hr)|
|------------|---------|---------|
|0 - 200     |15    |34|
|200 - 400   |15    |32|
|400 - 600   |15    |30|
|600 - 1000  |11.428|28|
|1000 - 1300 |13.333|26|

There are also manual test cases for the database features included in the `tests.txt` file within DockerMongo folder.